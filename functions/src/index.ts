import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp();
const db = admin.firestore();
export const loanCountListener = 
    functions.firestore.document('loans/{borrowerID}')
    .onCreate(async (change, context) => {
        try {
            // await db.doc('constants/loanCount').set({test: 'Hello World' });
            await db.doc('constants/loanCount').update({value: admin.firestore.FieldValue.increment(1)});
        } catch (error) {
            console.log('error', error);
        }
        return;
    });