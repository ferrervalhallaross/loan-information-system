// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDdZpYBdtY9T8HFiBI05SpyScjo9RY3ABM",
    authDomain: "loan-information-system.firebaseapp.com",
    databaseURL: "https://loan-information-system.firebaseio.com",
    projectId: "loan-information-system",
    storageBucket: "loan-information-system.appspot.com",
    messagingSenderId: "805201299649",
    appId: "1:805201299649:web:e67d0b4c31e8ba055e6c40",
    measurementId: "G-JF0H32V53Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
