import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private menu: MenuController,
        private router: Router,
        private storage: Storage
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.menu.enable(false, 'client');

            this.storage.get('oneSignalID').then(
                oneSignalID => {
                    console.log('oneSignalID', oneSignalID);
                    
                    if(oneSignalID) {
                        this.getUserId()
                    } else {
                        this.oneSignalInit();
                    }
                }
            )
        });
    }

    goTo(route) {
        this.router.navigate([route]);
        this.menu.close();
    }
    
    oneSignalInit() {
        var OneSignal = window['OneSignal'] || [];
        console.log("Init OneSignal");
        OneSignal.push(["init", {
            appId: "f1f4af9f-f280-4cb1-bb56-ee0be91eb016",
            autoRegister: false,
            // allowLocalhostAsSecureOrigin: true,
            notifyButton: {
                enable: false
            }
        }]);
        console.log('OneSignal Initialized');
        OneSignal.push(function () {
            console.log('Register For Push');
            OneSignal.push(["registerForPushNotifications"])
        });
        OneSignal.push(() => {
            OneSignal.getUserId().then((userId) => {
                console.log("User ID is", userId);
                this.storage.set('oneSignalID', userId);
            });
            // Occurs when the user's subscription changes to a new value.
            OneSignal.on('subscriptionChange', function (isSubscribed) {
                console.log("The user's subscription state is now:", isSubscribed);
            });
        });
    }

    getUserId() {
        var OneSignal = window['OneSignal'] || [];
        OneSignal.getUserId().then(async (userId) => {
            console.log("getUserId User ID is", userId);
            await this.storage.set('oneSignalID', userId);
        });
    }

}
