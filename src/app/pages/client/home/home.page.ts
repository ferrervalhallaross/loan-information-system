import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { FirebaseService } from 'src/app/providers/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    loading = false;
    profile: any = {};
    constructor(
        private auth: AuthService,
        private router: Router,
        private menu: MenuController,
        private fireService: FirebaseService,
        private authService: AuthService
    ) {
    }

    ngOnInit() {
        this.getProfile();
    }


    async getProfile() {
        this.loading = true;
        const borrowerID = await this.authService.getBorrowerId();
        this.fireService.getBorrowerInfo(borrowerID).then(data => {
            console.log('data', data);
            this.profile = data;
            this.loading = false;
        }).catch(error => {
            console.log('error', error);

        })
    }

    logout(){
        this.auth.storageRemoveBorrowerId().then(() => {
            this.auth.doLogout().then(() => {
                this.router.navigate(['login']);
            })
        })
    }

    apply() {
        console.log('apply loan');
        this.router.navigate(["/apply-loan"]);
        
    }

    toggleMenu() {
        this.menu.toggle('client');
    }
    goTo(route) {
        this.router.navigate([route]);
        this.menu.close();
    }

}
