import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';
import { AlertController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { MultiFileUploadComponent } from 'src/app/components/multi-file-upload/multi-file-upload.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseService } from 'src/app/providers/firebase.service';
import { AdminService } from 'src/app/providers/admin.service';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-apply-loan',
    templateUrl: './apply-loan.page.html',
    styleUrls: ['./apply-loan.page.scss'],
})
export class ApplyLoanPage implements OnInit {

    @ViewChild(MultiFileUploadComponent, { static: null }) fileField: MultiFileUploadComponent;
    validations_form: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';
    uploadProgress: any = 0;
    monthlyPayment = [];
    profile: any = {};

    validation_messages = {
        'monthsToPay': [
            { type: 'required', message: 'Loan Term is required.' },
            { type: 'min', message: 'Loan Term should be atleast 1.' },
        ],
        'principal': [
            { type: 'required', message: 'Loan Amount is required.' },
        ],
    };

    loading = false;
    minLoan = 1000;
    maxLoan = 10000

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private fireService: FirebaseService,
        private router: Router,
        private alert: AlertController,
        private fireStorage: AngularFireStorage,
        private afs: AngularFirestore,
        private adminService: AdminService,
        private storage: Storage
    ) { 
        this.getProfile();
    }

    async ngOnInit() {
        this.validations_form = this.formBuilder.group({
            amountToPay: new FormControl(0, Validators.compose([
                Validators.required, Validators.minLength(1)
            ])),
            borrowerID: new FormControl('', Validators.compose([
                Validators.required
            ])),
            dateCreated: new FormControl(new Date()),
            monthlyPayment: new FormControl(undefined, Validators.compose([
                Validators.required, Validators.min(1)
            ])),
            monthsToPay: new FormControl(3, Validators.compose([
                Validators.required, Validators.min(1)
            ])),
            principal: new FormControl(0, Validators.compose([
                Validators.required
            ])),
            rate: new FormControl(8),
            status: new FormControl('pending'),
            oneSignalID: new FormControl('')
        });
        this.getBorrowerId()

        const oneSignalID = await this.storage.get('oneSignalID');
        this.validations_form.controls['oneSignalID'].setValue(oneSignalID);
    }



    async getProfile() {
        this.loading = true;
        const borrowerID = await this.authService.getBorrowerId();
        this.fireService.getBorrowerInfo(borrowerID).then(data => {
            console.log('data', data);
            this.profile = data;

            switch (true) {
                case data.incomeMonthly > 100000:
                    this.maxLoan = 200000
                    break;
                case data.incomeMonthly > 75000:
                    this.maxLoan = 150000
                    break;
                case data.incomeMonthly > 50000:
                    this.maxLoan = 100000
                    break;
                case data.incomeMonthly > 25000:
                    this.maxLoan = 50000
                    break;
                case data.incomeMonthly > 20000:
                    this.maxLoan = 40000
                    break;
                case data.incomeMonthly > 15000:
                    this.maxLoan = 30000
                    break;
                case data.incomeMonthly > 10000:
                    this.maxLoan = 20000
                    break;
                case data.incomeMonthly > 5000:
                    this.maxLoan = 10000
                    break;
                case data.incomeMonthly > 3000:
                    this.maxLoan = 6000
                    break;
            
                default:
                    break;
            }
            this.loading = false;
        }).catch(error => {
            console.log('error', error);

        })
    }

    getBorrowerId() {
        this.authService.getBorrowerId().then(data => {
            this.validations_form.get('borrowerID').setValue(data);
        })
    }

    onPrincipalChange(principal) {
        const { monthsToPay, rate } = this.validations_form.value;
        console.log('here', monthsToPay, principal, rate);

        principal = parseInt(principal);
        this.computeSummary(principal, monthsToPay, rate);
        this.validations_form.get('principal').setValue(principal);
    }

    onMonthsToPay(monthsToPay) {
        
        const { principal, rate } = this.validations_form.value;

        monthsToPay = parseInt(monthsToPay);
        this.computeSummary(principal, monthsToPay, rate);
        this.validations_form.get('monthsToPay').setValue(monthsToPay);

    }

    computeSummary(principal, monthsToPay, rate) {

        // const amountToPay = principal + (principal * (rate * (monthsToPay / 12)) / 100);
        const amountToPay = principal*Math.pow((1 + (rate/100)), monthsToPay/12)
        // const amountToPay = principal + (principal * rate / 100);
        this.validations_form.get('amountToPay').setValue(Math.ceil(amountToPay));
        const monthlyPayment = amountToPay / monthsToPay;
        this.validations_form.get('monthlyPayment').setValue(Math.ceil(monthlyPayment));
        this.monthlyPayment = this.getMonths(monthsToPay);
    }

    getMonths(num) {
        // console.log('getMonths', array);
        let amount = Math.ceil(this.validations_form.get('monthlyPayment').value);
        let dates = [{
            date: new Date(),
            amount
        },
        ]
        for (let i = 0; i < num - 1; i++) {
            let now = dates[i].date;
            let day = now.getDate();
            // console.log('here', now, day, (new Date()).getDate() );

            let nextDate = new Date(now.getFullYear(), now.getMonth() + 1, day);
            if (num > 1 && i == num - 2) { // ibigsabihin last na
                let lastAmount = this.validations_form.get('amountToPay').value - (amount * (num - 1));
                dates = [...dates, { date: nextDate, amount: lastAmount }];
            } else {
                dates = [...dates, { date: nextDate, amount }];
            }
        }
        return dates;
    }

    async apply() {
        console.log('here', this.validations_form.value);


        this.loading = true;
        let value = this.validations_form.value;
        value.profile = await this.fireService.getBorrowerInfo(value.borrowerID);
        value.accountNo = await this.generateAccountNo();

        this.authService.addLoan(value).then(async data => {
            console.log("Success add loan", data);
            this.showAlert("Your loan is waiting for approval");
            const adminOneSignalID = await this.storage.get("adminOneSignalID");
            this.adminService.postNotif(adminOneSignalID, "There's a new loan application!").then(data => {
                console.log('onesignal', data);
            }).catch(error => {
                console.log('error onesignal', error);
            })
            this.loading = false;
        }).catch(error => {
            // this.errorMessage = error.message;
            this.showAlert(error.message);
            this.loading = false;
        })
    }

    async generateAccountNo() {
        let count = await this.authService.getLoanCount();
        count = count.data().value
        let date = new Date();
        return date.getFullYear() + '' + this.paddy(date.getMonth() + 1, 2) + '' + this.paddy(count, 6)

    }

    paddy(num, padlen, padchar = '0') {
        var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
        var pad = new Array(1 + padlen).join(pad_char);
        return (pad + num).slice(-pad.length);
    }

    showAlert(message) {
        this.alert.create({
            message,
            buttons: [
                {
                    text: 'Okay',
                    role: 'Cancel',
                    handler: () => {
                        console.log('Confirm Okay');
                    }
                }
            ]
        }).then(alert => {
            alert.present();
        })
    }

    onAmountChange(e) {
        console.log('onAmountChange($event)', e);
        this.validations_form.controls['principal'].setValue(e.detail.value);
        this.onPrincipalChange(e.detail.value)
        
    }

    onMonthChange(e) {
        this.validations_form.controls['monthsToPay'].setValue(e.detail.value);
        this.onMonthsToPay(e.detail.value)
    }
}