import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplyLoanPageRoutingModule } from './apply-loan-routing.module';

import { ApplyLoanPage } from './apply-loan.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ApplyLoanPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ApplyLoanPage]
})
export class ApplyLoanPageModule {}
