import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgForm, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/providers/auth.service';
import { FirebaseService } from 'src/app/providers/firebase.service';
import { AdminService } from 'src/app/providers/admin.service';
import * as firebase from 'firebase/app';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    validations_form: FormGroup;
    errorMessage: string = '';
    loading = false;

    validation_messages = {
        'email': [
            { type: 'required', message: 'Email is required.' },
            { type: 'pattern', message: 'Please enter a valid email.' }
        ],
        'password': [
            { type: 'required', message: 'Password is required.' },
            { type: 'minlength', message: 'Password must be at least 5 characters long.' }
        ]
    };

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private router: Router,
        private alert: AlertController,
        private storage: Storage,
        private menu: MenuController,
        private fireService: FirebaseService,
        private adminService: AdminService
    ) {
    }

    ngOnInit() {
        this.menu.enable(false, 'client');
        this.menu.enable(false, 'admin');
        this.validations_form = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.required
            ])),
        });
    }

    tryLogin(value) {
        this.loading = true;
        this.authService.doLogin(value)
            .then(res => {
                console.log('Succes Login', res);
                console.log('isEmailVerified', res.user.emailVerified);
                this.authService.getAdminEmail().then(data => {
                    // console.log("data", data);
                    data.forEach((doc) => {
                        // console.log("doc data", doc.data());
                        if (res.user.email == doc.data().email) {
                            this.loading = false;
                            this.storage.set('isAdmin', true).then(async () => {

                                this.menu.enable(true, 'admin');
                                this.menu.enable(false, 'client');
                                this.router.navigate(["/loan-applications"]);
                                const oneSignalID = await this.storage.get('oneSignalID');
                                if (oneSignalID) {
                                    this.fireService.updateOneSignalIDAdmin(oneSignalID);
                                    await this.storage.set('adminOneSignalID', oneSignalID)
                                } else {
                                    // this.oneSignalInit(data.docs[0].id, true);
                                }
                            })
                        } else {
                            return this.checkIfBorrower(value.email, res.user.emailVerified);
                        }
                    });
                }).catch(error => {
                    this.loading = false;
                    console.log('Error', error);
                    this.errorMessage = error.message;
                });
            }, err => {
                this.loading = false;
                console.log('Error login', err);
                this.errorMessage = err.message;
                console.log(err)
            })
    }

    // oneSignalInit(borrowerId, isAdmin = false) {
    //     var OneSignal = window['OneSignal'] || [];
    //     console.log("Init OneSignal");
    //     OneSignal.push(["init", {
    //         appId: "f1f4af9f-f280-4cb1-bb56-ee0be91eb016",
    //         autoRegister: false,
    //         allowLocalhostAsSecureOrigin: true,
    //         notifyButton: {
    //             enable: false
    //         }
    //     }]);
    //     console.log('OneSignal Initialized');
    //     OneSignal.push(function () {
    //         console.log('Register For Push');
    //         OneSignal.push(["registerForPushNotifications"])
    //     });
    //     OneSignal.push(() => {
    //         OneSignal.getUserId().then((userId) => {
    //             if(userId) {
    //                 localStorage.setItem('oneSignalID', userId);
    //             }
    //             if(isAdmin) {

    //                 localStorage.setItem('adminOneSignalID', userId);
    //                 this.fireService.updateOneSignalIDAdmin(userId);
    //             } else {
    //                 this.fireService.updateOneSignalID(borrowerId, userId);
    //             }
    //         });
    //         // Occurs when the user's subscription changes to a new value.
    //         OneSignal.on('subscriptionChange', function (isSubscribed) {
    //             console.log("The user's subscription state is now:", isSubscribed);
    //         });
    //     });
    // }

    checkIfBorrower(email, isEmailVerified) {
        this.authService.checkIfBorrower(email).then(data => {
            console.log('check if company', data.docs, email);
            if (data.size) {
                if (data.docs[0].data().inactive) {
                    this.loading = false;
                    return this.showAlert('This account is deactivated. Please contact administrator if you want to activate this account.');
                }
                this.loading = false;
                this.storage.set('borrowerId', data.docs[0].id).then(async () => {
                    this.menu.enable(false, 'admin');
                    this.menu.enable(true, 'client');
                    const oneSignalID = await this.storage.get('oneSignalID');
                    if (oneSignalID) {
                        this.fireService.updateOneSignalID(data.docs[0].id, oneSignalID);
                    } else {
                        // this.oneSignalInit(data.docs[0].id);
                    }
                    this.adminService.getAdminOneSignalID().then(data => {
                        data.forEach(async (doc) => {
                            // console.log("doc data", doc.data());
                            if (doc.data().oneSignalID) {
                                await this.storage.set('adminOneSignalID', doc.data().oneSignalID)
                            }
                        });
                    }).catch(error => {
                        console.log('error');
                    })

                    if (isEmailVerified) {
                        this.router.navigate(["/home"]);
                    } else {
                        this.unverifiedAlert();
                    }
                })
            } else {
                this.loading = false;
                return this.showAlert('This account no longer exists');
            }
        }).catch(error => {
            console.log('Error', error);
        });
    }

    async unverifiedAlert() {
        const alert = await this.alert.create({
            cssClass: 'my-custom-class',
            header: 'Unverified Email!',
            message: 'Please verify your email before sign in!!!',
            buttons: [
                {
                    text: 'Resend',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        firebase.auth().currentUser.sendEmailVerification();

                        firebase.auth().signOut();
                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                        console.log('Confirm Okay');
                    }
                }
            ]
        });

        await alert.present();
    }

    goRegisterPage() {
        this.router.navigate(["/signup"]);
    }

    showAlert(msg) {
        this.alert.create({
            message: msg
        }).then(alert => {
            alert.present();
        })
    }
}