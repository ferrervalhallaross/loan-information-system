import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-loans',
  templateUrl: './loans.page.html',
  styleUrls: ['./loans.page.scss'],
})
export class LoansPage implements OnInit {
  loading = false;
  loans = [];
  constructor(
    private router: Router,
    private storage: Storage,
    public afAuth: AngularFireAuth,
    private zone: NgZone,
  ) { }

  ngOnInit() {
    this.listenToBusList();
  }

  add() {
    console.log('add');
    this.router.navigate(["/apply-loan"]);
  }

  async listenToBusList() {
    this.loading = true;
    const borrowerID = await this.storage.get('borrowerId');
    firebase.firestore().collection('loans').where("borrowerID", "==", borrowerID)
    .onSnapshot((querySnapshot) => {
        const data = []
        querySnapshot.forEach(function(doc) {
            data.push({...doc.data(), _id: doc.id})
        });
        console.log('new data', data);
        this.zone.run(() => {
            this.loading = false;
            this.loans = data
        })
    })
  }

  view(loan) {
    let navigationExtras: NavigationExtras = {
      state: {
        loan,
        isClient: true
      }
    };
    this.router.navigate(['loan-view'], navigationExtras);
  }
  
}
