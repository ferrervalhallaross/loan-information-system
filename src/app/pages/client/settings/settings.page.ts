import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router,
    private menu: MenuController
  ) { }

  ngOnInit() {
  }


  logout(){
    this.auth.storageRemoveIsAdmin().then(() => {
        this.auth.storageRemoveBorrowerId().then(() => {
            this.auth.doLogout().then(() => {
                this.menu.enable(false, 'client');
                this.router.navigate(['login']);
            })
        })
    });
  }
  

}
