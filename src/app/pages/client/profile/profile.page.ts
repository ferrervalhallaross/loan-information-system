import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/providers/firebase.service';
import { AuthService } from 'src/app/providers/auth.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    loading = false;
    profile: any = {};
    constructor(
        private fireService: FirebaseService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.getProfile();
    }


    async getProfile() {
        this.loading = true;
        const borrowerID = await this.authService.getBorrowerId();
        this.fireService.getBorrowerInfo(borrowerID).then(data => {
            console.log('data', data);
            this.profile = data;
            this.loading = false;
        }).catch(error => {
            console.log('error', error);

        })
    }
}
