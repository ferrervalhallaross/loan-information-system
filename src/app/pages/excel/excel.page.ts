import { Component, OnInit, NgZone } from '@angular/core';
import * as XLSX from 'xlsx';
type AOA = any[][];

@Component({
  selector: 'app-excel',
  templateUrl: './excel.page.html',
  styleUrls: ['./excel.page.scss'],
})
export class ExcelPage {
  data: AOA = [ [1, 2], [3, 4] ];
	wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  
  rows: any = [
  ];

  columns: any = [
  ]; 
  
  constructor(
    private zone: NgZone,
  ) { }

	onFileChange(evt: any) {
		/* wire up file reader */
		const target: DataTransfer = <DataTransfer>(evt.target);
		if (target.files.length !== 1) throw new Error('Cannot use multiple files');
		const reader: FileReader = new FileReader();
		reader.onload = (e: any) => {
			/* read workbook */
			const bstr: string = e.target.result;
			const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

			/* grab first sheet */
			const wsname: string = wb.SheetNames[0];
			const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      console.log('here', XLSX.utils.sheet_to_json(ws, {header: 1}));
      
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.genRowCol(this.data);
		};
		reader.readAsBinaryString(target.files[0]);
	}

  genRowCol(data) {
    console.log('here', data[0]);
    
    this.columns = data[0].map(col => {
      console.log(col);
      return { name: col }
    })
    console.log('columns', this.columns);
    
    this.rows = data.slice(1).map((col, i) => {
      console.log('col', col, i);
      let row = {};
      this.columns.forEach((element, i) => {
        row[element.name.toLowerCase()] = col[i];
      });
      console.log('row', row);
      return row
      
    })

    // this.zone.run(() => {
      this.columns = [...this.columns];
      this.rows = [...this.rows];
      console.log('rows', this.rows);
    // })
  }

	export(): void {
		/* generate worksheet */
		const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

		/* generate workbook and add the worksheet */
		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

		/* save to file */
		XLSX.writeFile(wb, this.fileName);
	}

}
