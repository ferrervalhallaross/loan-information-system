import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CouplePage } from './couple.page';

const routes: Routes = [
  {
    path: '',
    component: CouplePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CouplePageRoutingModule {}
