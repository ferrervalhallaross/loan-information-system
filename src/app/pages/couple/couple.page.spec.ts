import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CouplePage } from './couple.page';

describe('CouplePage', () => {
  let component: CouplePage;
  let fixture: ComponentFixture<CouplePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CouplePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CouplePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
