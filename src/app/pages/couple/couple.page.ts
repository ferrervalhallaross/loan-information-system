import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-couple',
  templateUrl: './couple.page.html',
  styleUrls: ['./couple.page.scss'],
})
export class CouplePage implements OnInit {
    
    couples = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15]
    success = 0;
    fail = 0;
    n = 10000000
    constructor() { }

    ngOnInit() {
        for (let i = 0; i < this.n; i++) {
            this.simulate();
            this.couples = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15];
        }
        console.log("chance ", (this.success/(this.success+this.fail)));
        
    }

    simulate() {
        for (let i = 0; i < 28; i++) {
            let random = this.getRandomInt(0, this.couples.length - 1);
            this.couples.splice(random, 1);
        }
        if(this.isCouple()){
            this.success += 1;
        } else {
            this.fail += 1;
        }
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    isCouple(){
        if(this.couples[0] == this.couples[1]) {
            return true;
        }
        return false
    }


    

}
