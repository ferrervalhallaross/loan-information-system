import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CouplePageRoutingModule } from './couple-routing.module';

import { CouplePage } from './couple.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CouplePageRoutingModule
  ],
  declarations: [CouplePage]
})
export class CouplePageModule {}
