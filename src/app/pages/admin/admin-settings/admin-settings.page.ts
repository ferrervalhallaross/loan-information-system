import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.page.html',
  styleUrls: ['./admin-settings.page.scss'],
})
export class AdminSettingsPage implements OnInit {

    constructor(
        private auth: AuthService,
        private router: Router,
        private menu: MenuController
    ) { }

    ngOnInit() {
    }

  logout(){
    this.auth.storageRemoveIsAdmin().then(() => {
        this.auth.storageRemoveBorrowerId().then(() => {
            this.auth.doLogout().then(() => {

                this.menu.enable(false, 'admin');
                this.router.navigate(['login']);
            })
        })
    });
  }
  }