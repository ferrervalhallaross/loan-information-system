import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanApplicationsPage } from './loan-applications.page';

const routes: Routes = [
  {
    path: '',
    component: LoanApplicationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanApplicationsPageRoutingModule {}
