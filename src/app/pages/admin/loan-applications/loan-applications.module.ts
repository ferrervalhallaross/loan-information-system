import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanApplicationsPageRoutingModule } from './loan-applications-routing.module';

import { LoanApplicationsPage } from './loan-applications.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    LoanApplicationsPageRoutingModule,
    ComponentsModule
  ],
  declarations: [LoanApplicationsPage]
})
export class LoanApplicationsPageModule {}
