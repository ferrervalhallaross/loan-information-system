import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-loan-applications',
  templateUrl: './loan-applications.page.html',
  styleUrls: ['./loan-applications.page.scss'],
})
export class LoanApplicationsPage implements OnInit {
  loading = false;
  loans = [];
  loansFiltered = [];
  searchTerm = '';

  constructor(
    public afAuth: AngularFireAuth,
    private zone: NgZone,
    private router: Router
  ) { }

  ngOnInit() {
    this.listenToLoans();
  }

  async listenToLoans() {
    this.loading = true;
    firebase.firestore().collection('loans').where("status", "==", 'pending').orderBy('dateCreated', 'desc')
    .onSnapshot((querySnapshot) => {
        const data = []
        querySnapshot.forEach(function(doc) {
            data.push({...doc.data(), _id: doc.id, fullName: doc.data().profile.firstName + ' ' + ( doc.data().profile.middleName ? doc.data().profile.middleName + ' ' : '' )  + doc.data().profile.lastName})
        });
        console.log('new data', data);
        this.zone.run(() => {
            this.loading = false;
            this.loans = data
        })
        this.setFilteredItems();
    })
  }

  setFilteredItems() {
      this.loansFiltered = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm) {
    return this.loans.filter(item => {
        return (item.fullName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || item.accountNo.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
    });
  }


  view(loan) {
    let navigationExtras: NavigationExtras = {
      state: {
        loan
      }
    };
    this.router.navigate(['loan-view'], navigationExtras);
  }
}
