import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanHistoryPageRoutingModule } from './loan-history-routing.module';

import { LoanHistoryPage } from './loan-history.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoanHistoryPageRoutingModule,
    ComponentsModule
  ],
  declarations: [LoanHistoryPage]
})
export class LoanHistoryPageModule {}
