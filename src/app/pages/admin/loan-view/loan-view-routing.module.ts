import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanViewPage } from './loan-view.page';

const routes: Routes = [
  {
    path: '',
    component: LoanViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanViewPageRoutingModule {}
