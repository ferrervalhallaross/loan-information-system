import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from 'src/app/providers/firebase.service';
import { AdminService } from 'src/app/providers/admin.service';
import { AlertController, ModalController } from '@ionic/angular';
import { PostCollectionPage } from '../post-collection/post-collection.page';
import * as firebase from 'firebase/app';
import * as jspdf from 'jspdf';  
import { DatePipe } from '@angular/common';
import { FLAG_UNRESTRICTED } from 'html2canvas/dist/types/css/syntax/tokenizer';

@Component({
    selector: 'app-loan-view',
    templateUrl: './loan-view.page.html',
    styleUrls: ['./loan-view.page.scss'],
    providers: [DatePipe],
})
export class LoanViewPage implements OnInit {
    loading = false;
    @Input('loan') loan: any = {}
    monthlyPayment = [];
    profile: any = {};
    //continue here
    actions = true;
    collection = [];
    totalCollection = 0;
    isClient = false;

    balance = 0;
    pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fireService: FirebaseService,
        private adminService: AdminService,
        private alert: AlertController,
        private modal: ModalController,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef
    ) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.loan = this.router.getCurrentNavigation().extras.state.loan;
                this.isClient = this.router.getCurrentNavigation().extras.state.isClient;
                console.log('loan', this.loan);
                this.listenToCollection();
                this.getProfile();
            } else {
                this.router.navigate(["/loan-applications"]);
            }
        });
    }

    ngOnInit() {
    }

    getProfile() {
        this.fireService.getBorrowerInfo(this.loan.borrowerID).then(data => {
            console.log('data', data);
            this.profile = data;
        }).catch(error => {
            console.log('error', error);

        })
    }

    computeSummary() {
        let { principal, monthsToPay, rate } = this.loan;
        // const amountToPay = principal + (principal * (rate * (monthsToPay / 12)) / 100);

        const amountToPay = principal*Math.pow((1 + (rate/100)), monthsToPay/12)
        this.monthlyPayment = this.getMonths(monthsToPay);
        this.monthlyPayment = this.computePenalty(this.monthlyPayment);
        // this.loading = false
    }

    computePenalty(monthlyPayment: Array<any>) {
        let mPaymentWithPenalty = []
        monthlyPayment.map((payment, i) => {
            const { amount, penalty, progress } = this.lessCollection(payment);
            payment.amount = amount;
            payment.penalty = penalty;
            payment.progress = progress;

            if (penalty) {
                const { amount, penalty, progress } = this.lessCollection(payment.penalty);
                payment.penalty.amount = amount;
                payment.penalty.progress = progress;
            }
            // let progress = this.getProgress(payment.amount)
            if (payment.progress == 1 && i == this.monthlyPayment.length - 1 && this.loan.status == 'approved') {
                this.done();
            }
            mPaymentWithPenalty.push(payment)
        })
        return mPaymentWithPenalty
    }

    lessCollection(toPay, isPenalty = false) {
        const dateToPay = new Date(toPay.date);
        const dateNow = new Date();
        const initialPaymentAmount = toPay.amount;
        let penalty

        this.collection = this.collection.map(collect => {

            let dateCollected = new Date(collect.date);

            // kung ung date of collection is greater or equal sa date to pay
            if ((0 <= (dateToPay.getTime() - dateCollected.getTime()) / 86400000) || isPenalty) {

                toPay.amount -= collect.amount - (collect.lessed || 0);
                if (toPay.amount < 0) {
                    collect.lessed = collect.amount + toPay.amount
                    toPay.amount = 0
                } else {
                    collect.lessed = (collect.lessed || 0) + collect.amount - (collect.lessed || 0);
                }
            } else { //meaning collection ito after ng date to pay so dapat may penalty na

                if (toPay.amount > 0) {
                    let penaltyCount = Math.ceil((dateCollected.getTime() - dateToPay.getTime()) / 2628000000)
                    penalty = { ...toPay, penalty: true, penaltyCount };
                    penalty.amount = Math.ceil(initialPaymentAmount * 0.08 * penaltyCount)
                }

                toPay.amount -= collect.amount - (collect.lessed || 0);
                if (toPay.amount < 0) {
                    collect.lessed = collect.amount + toPay.amount
                    toPay.amount = 0
                } else {
                    collect.lessed = (collect.lessed || 0) + collect.amount - (collect.lessed || 0);
                }
            }
            return collect
        })
        //kung kulang pa payment at lagpas na yun date
        if (toPay.amount > 0 && 1 <= (dateNow.getTime() - dateToPay.getTime()) / 86400000) {
            let penaltyCount = Math.ceil((dateNow.getTime() - dateToPay.getTime()) / 2628000000)
            penalty = { ...toPay, penalty: true, penaltyCount };
            penalty.amount = Math.ceil(initialPaymentAmount * 0.08 * penaltyCount)

        }
        return { amount: toPay.amount, penalty, progress: (initialPaymentAmount - toPay.amount) / initialPaymentAmount }
    }

    getTotalCollection(array) {
        let total = 0
        array.map(collection => {
            total += collection.amount
        })
        return total
    }

    async listenToCollection() {
        this.loading = true;
        this.cdRef.detectChanges();
        firebase.firestore().collection('collection').where("accountNo", "==", this.loan.accountNo)
            .onSnapshot((querySnapshot) => {
                const data = []
                this.loading = false;
                this.cdRef.detectChanges();
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id })
                });
                // console.log('new data', data.sort((a, b) => { 
                //     let date1 = new Date(a.date);
                //     let date2 = new Date(b.date);
                //     console.log('im here', date2.getTime() - date1.getTime());

                //     return date1.getTime() - date2.getTime()

                // }) );
                this.collection = data.sort((a, b) => {
                    let date1 = new Date(a.date);
                    let date2 = new Date(b.date);
                    return date1.getTime() - date2.getTime()

                })
                this.totalCollection = this.getTotalCollection(this.collection);
                console.log('total', this.totalCollection);

                this.computeSummary();

                this.cdRef.detectChanges();
            })
    }

    getMonths(num) {
        // console.log('getMonths', array);
        let { monthlyPayment, amountToPay, dateCreated } = this.loan;
        let amount = monthlyPayment;
        // let progress = this.getProgress(amount)

        let newDateCreated = new Date(dateCreated.toDate());
        newDateCreated.setMonth(newDateCreated.getMonth() + 1);
        let dates = [{
            date: newDateCreated,
            amount
            // amount: amount - amount * progress,
            // progress
        },
        ]
        for (let i = 0; i < num - 1; i++) {
            let now = new Date(dates[i].date);
            let day = now.getDate();
            // console.log('here', now, day, (new Date()).getDate() );

            now.setMonth(now.getMonth() + 1);
            if (num > 1 && i == num - 2) { // ibigsabihin last na
                amount = amountToPay - (amount * (num - 1));
            }
            // progress = this.getProgress(amount)

            dates = [...dates, { date: now, amount }];


        }
        return dates;
    }

    getProgress(amount) {
        let progress = 0;
        if (this.totalCollection > amount) {
            progress = 1
            this.totalCollection -= amount
        } else {
            progress = this.totalCollection / amount
            this.totalCollection = 0
        }
        return progress
    }

    approve() {
        this.adminService.approveLoan(this.loan._id).then((data) => {
            this.showAlert('The loan was successfuly approved');

            this.adminService.postNotif(this.profile.oneSignalID, "Youre loan is approved!").then(data => {
                console.log('onesignal', data);
            }).catch(error => {
                console.log('error onesignal', error);
            })

            this.monthlyPayment.map(bill => {
                this.adminService.postNotif(this.profile.oneSignalID, `Please pay your monthly billing amounting ${bill.amount}`, bill.date.getTime(), 'billing').then(data => {
                    console.log('onesignal', data);
                }).catch(error => {
                    console.log('error onesignal', error);
                })
            })
            this.router.navigate(["/loan-active"]);
        }).catch(error => {
            this.showAlert('Error occured. Please try again later');
        })
    }

    done() {
        this.adminService.doneLoan(this.loan._id).then((data) => {
            this.showAlert('The loan was finished');
            this.router.navigate(["/loan-active"]);
        }).catch(error => {
            this.showAlert('Error occured. Please try again later');
        })
    }

    decline() {
        this.adminService.declineLoan(this.loan._id).then((data) => {
            this.showAlert('The loan was successfuly declined');
            this.adminService.postNotif(this.profile.oneSignalID, "Youre loan is declined.").then(data => {
                console.log('onesignal', data);
            }).catch(error => {
                console.log('error onesignal', error);
            })
            this.router.navigate(["/loan-history"]);
        }).catch(error => {
            this.showAlert('Error occured. Please try again later');
        })
    }


    showAlert(message) {
        this.alert.create({
            message,
            buttons: [
                {
                    text: 'Okay',
                    role: 'Cancel',
                    handler: () => {
                        console.log('Confirm Okay');
                    }
                }
            ]
        }).then(alert => {
            alert.present();
        })
    }

    async showPostForm() {
        const alert = await this.alert.create({
            header: 'Prompt!',
            backdropDismiss: false,
            inputs: [
                {
                    name: 'date',
                    id: 'date',
                    type: 'date',
                },
                {
                    name: 'amount',
                    type: 'number',
                    id: 'amount',
                    placeholder: 'Amount',
                    min: 1,
                },
                {
                    name: 'remarks',
                    id: 'remarks',
                    type: 'textarea',
                    placeholder: 'remarks'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (data) => {
                        console.log('Confirm Cancel', data);
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        console.log('Confirm Ok', data);
                    }
                }
            ]
        });

        await alert.present();
    }

    async showPostCollectionModal() {
        console.log('this.loan', this.loan);

        const modal = await this.modal.create({
            component: PostCollectionPage,
            componentProps: {
                accountNo: this.loan.accountNo,
                loanID: this.loan._id,
                borrowerID: this.loan.borrowerID,
            }
        });
        modal.onDidDismiss().then(data => {
            if (data.data.success) {
                this.showAlert("Collection has been posted");
            }
        });
        return await modal.present();
    }

    async notify() {
        console.log('this', this.loan.profile);

        let alert = await this.alert.create({
            message: 'Notification',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Please pay your monthly billing'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: data => {
                        this.adminService.postNotif(this.profile.oneSignalID, data.message).then(data => {
                            console.log('onesignal', data);
                        }).catch(error => {
                            console.log('error onesignal', error);
                        })
                    }
                }
            ]
        });
        alert.present();
    }

    async cancel() {
        const alert = await this.alert.create({
            // cssClass: 'my-custom-class',
            header: 'Confirm!',
            message: 'Are you sure to <strong>cancel</strong> this loan!!!',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                        console.log('Confirm Okay');
                        this.adminService.cancelLoan(this.loan._id).then((data) => {
                            this.showAlert('The loan was successfuly canceled');
                            this.adminService.postNotif(this.profile.oneSignalID, "Youre loan is cancelled.").then(data => {
                                console.log('onesignal', data);
                            }).catch(error => {
                                console.log('error onesignal', error);
                            })
                            this.router.navigate(["/loan-history"]);
                        }).catch(error => {
                            this.showAlert('Error occured. Please try again later');
                        })
                    }
                }
            ]
        });

        await alert.present();
    }

    print() {
        let y = 15;
        this.fontHeader();
        this.pdf.text('Loan Information System', 10, y);
        this.small()
        this.pdf.text(`Rogado Lending`, 95, y); 
        this.pdf.text(`Date Generated: ${this.datePipe.transform(Date.now())}`, 150, y); 
        y = this.nextline(y, 2);

        this.fontHeader();
        this.pdf.text('Loan Details', 10, y); y = this.nextline(y);

        this.fontNormal();
        this.pdf.text(`Amount: PHP ${this.loan.principal}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Account Number: ${this.loan.accountNo}`, 10, y); y = this.nextline(y);
        this.pdf.text(`Loan Term (Months to Pay): ${this.loan.monthsToPay} months`, 10, y); y = this.nextline(y);
        this.pdf.text(`Monthly Payment: ${this.loan.monthlyPayment}`, 10, y); y = this.nextline(y);
        this.pdf.text(`Date of Application: ${this.datePipe.transform(this.loan.dateCreated.toDate())}`, 10, y); y = this.nextline(y, 2);
        
        this.fontHeader();
        this.pdf.text('Loan Summary', 10, y); y = this.nextline(y);

        this.fontNormal();
        this.pdf.text(`Monthly Payment: PHP ${this.loan.monthlyPayment}`, 10, y); y = this.nextline(y);
        // if (this.isClient) {
        this.pdf.text(`Remaining Balance: PHP ${this.loan.amountToPay - this.totalCollection}`, 10, y); y = this.nextline(y);
        this.pdf.text(`Total Payment: PHP ${this.totalCollection}`, 10, y); y = this.nextline(y, 2);
        // } else {
        // this.pdf.text(`Total Payment: PHP ${this.loan.amountToPay}`, 10, y); y = this.nextline(y);
        // }
        

        this.fontHeader();
        this.pdf.text('Profile', 10, y); y = this.nextline(y);

        this.fontNormal();
        this.pdf.text(`Name: ${this.profile.firstName + ' ' + this.profile.middleInitial + ' ' + this.profile.lastName}`, 10, y);  y = this.nextline(y); 
        this.pdf.text(`Address: ${this.profile.address}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Contact Number: ${this.profile.mobileNumber}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Email: ${this.profile.email}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Income: ${this.profile.incomeMonthly}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Guarantor Name: ${this.profile.guarantorName}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Guarantor Relation: ${this.profile.guarantorRelation}`, 10, y);  y = this.nextline(y);
        this.pdf.text(`Guarantor Mobile Number: ${this.profile.guarantorMobile}`, 10, y);  y = this.nextline(y, 2);



        this.fontHeader();
        this.pdf.text('Payment Schedules', 10, y); y = this.nextline(y);

        this.fontNormal();
        this.monthlyPayment.forEach(month => {
            this.pdf.text(`${this.datePipe.transform(month.date)}: PHP ${month.amount}`, 10, y); y = this.nextline(y);

            if(month.penalty) {
                this.pdf.text(`Penalty: PHP ${month.penalty.amount}`, 10, y); y = this.nextline(y);
            }
        });
        y = this.nextline(y);



        this.fontHeader();
        this.pdf.text('Payments', 10, y); y = this.nextline(y);

        this.fontNormal();
        this.collection.forEach(payment => {
            this.pdf.text(`${this.datePipe.transform(payment.date)}: PHP ${payment.amount}`, 10, y); y = this.nextline(y);

        });
        y = this.nextline(y);
        
        this.pdf.save('MYPdf.pdf'); // Generated PDF   
    }

    fontNormal() {
        this.pdf.setFontSize(14);
        this.pdf.setFontStyle('normal');
    }

    fontHeader() {
        this.pdf.setFontSize(12);
        this.pdf.setFontStyle('bold');
    }

    small() {
        this.pdf.setFontSize(10);
        this.pdf.setFontStyle('normal');
    }

    nextline(y, n = 1) {
        return y + (6 * n);
    }

    

}