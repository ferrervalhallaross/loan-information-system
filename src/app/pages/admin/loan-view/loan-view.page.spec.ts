import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoanViewPage } from './loan-view.page';

describe('LoanViewPage', () => {
  let component: LoanViewPage;
  let fixture: ComponentFixture<LoanViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoanViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
