import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanViewPageRoutingModule } from './loan-view-routing.module';

import { LoanViewPage } from './loan-view.page';
import { PostCollectionPage } from '../post-collection/post-collection.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoanViewPageRoutingModule,
    ComponentsModule
  ],
  declarations: [LoanViewPage],
  entryComponents: [
  ]
})
export class LoanViewPageModule {}
