import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    constructor(
        private auth: AuthService,
        private router: Router,
        private menu: MenuController
    ) { }

    ngOnInit() {
    }

    logout(){
        this.auth.storageRemoveIsAdmin().then(() => {
            this.auth.doLogout().then(() => {

                this.menu.enable(false, 'admin');
                this.router.navigate(['login']);
            })
        })
    }

}
