import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostCollectionPage } from './post-collection.page';

describe('PostCollectionPage', () => {
  let component: PostCollectionPage;
  let fixture: ComponentFixture<PostCollectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCollectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostCollectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
