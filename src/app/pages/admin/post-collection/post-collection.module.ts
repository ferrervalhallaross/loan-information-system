import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostCollectionPageRoutingModule } from './post-collection-routing.module';

import { PostCollectionPage } from './post-collection.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PostCollectionPageRoutingModule,
    ComponentsModule
  ],
  declarations: []
})
export class PostCollectionPageModule {}
