import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostCollectionPage } from './post-collection.page';

const routes: Routes = [
  {
    path: '',
    component: PostCollectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostCollectionPageRoutingModule {}
