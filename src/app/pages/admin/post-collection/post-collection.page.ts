import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { AdminService } from 'src/app/providers/admin.service';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-post-collection',
  templateUrl: './post-collection.page.html',
  styleUrls: ['./post-collection.page.scss'],
})
export class PostCollectionPage implements OnInit {

    validations_form: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';
    uploadProgress: any = 0;
    dateToday = '';
    validation_messages = {
      'date': [
        { type: 'required', message: 'Date is required.' },
      ],
      'amount': [
        { type: 'required', message: 'Amount is required.' },
        { type: 'minlength', message: 'Amount must be at least ₱1.00' }
      ],
    };
    @Input() borrowerID: string = '';
    @Input() accountNo: string = '';
    @Input() loanID: string = '';
  
    constructor(
      private formBuilder: FormBuilder,
      private alert: AlertController,
      private adminService: AdminService,
      private modal: ModalController
    ) {
        this.getToday();
        console.log('here', this.accountNo, this.borrowerID);
        
     }
     getToday() {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        
        this.dateToday = year + "-" + (month < 10 ? '0' : '') + month+ "-" + (day < 10 ? '0' : '') + day ;
        console.log(this.dateToday);
        
     }
    ngOnInit() {
      this.validations_form = this.formBuilder.group({
        date: new FormControl(Date.now(), Validators.compose([
          Validators.required,
        ])),
        amount: new FormControl('', Validators.compose([
          Validators.min(1),
          Validators.required
        ])),
        borrowerID: new FormControl(this.borrowerID, Validators.compose([
            // Validators.required,
        ])),
        accountNo: new FormControl(this.accountNo, Validators.compose([
            // Validators.required
        ])),
        loanID: new FormControl(this.loanID, Validators.compose([
            // Validators.required
        ])),
      });
    }
    
    submit(){
        this.adminService.postCollection(this.validations_form.value).then(data => {
                console.log("Success post collection", data);
                this.modal.dismiss({success: true});
        }).catch( error => {
                // this.errorMessage = error.message;
                this.showAlert(error.message);
        })
    }
  
    showAlert(message) {
      this.alert.create( {
        message,
        buttons: [
          {
            text: 'Okay',
            role: 'Cancel',
            handler: () => {
              console.log('Confirm Okay');
            }
          }
        ]
      }).then(alert => {
        alert.present();
      })
    }

    close() {
        this.modal.dismiss();
    }
  }