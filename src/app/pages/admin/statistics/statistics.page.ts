import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { NavigationExtras, Router } from '@angular/router';
import { Chart } from "chart.js";
import { AdminService } from 'src/app/providers/admin.service';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.page.html',
    styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {
    @ViewChild("lineCanvas", { static: true }) lineCanvas: ElementRef;

    private lineChart: Chart;
    loading = false;
    loans = [];
    collections = [];
    loansFiltered = [];
    collectionsFiltered = [];
    totalCapital = 0;
    totalCollection = 0;
    totalIncome = 0;
    dateToday = '';
    date = '';
    incomeMonthly = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    capitalMonthly = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    collectionMonthly = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    constructor(
        public afAuth: AngularFireAuth,
        private zone: NgZone,
        private router: Router,
        private adminService: AdminService
    ) { }

    ngOnInit() {
        this.listenToLoans();
        this.listenToCollection();
        this.getToday();
    }

    dateChange(event) {
        console.log('event', this.date.substring(0, 4));
        this.filterLoans();
        this.filterCollection();
    }

    filterLoans() {
        this.loansFiltered = this.loans.filter(loan => {
            // console.log('toDate', '' + loan.dateCreated.toDate().getUTCFullYear());
            return '' + loan.dateCreated.toDate().getUTCFullYear() == this.date.substring(0, 4);
        });

        this.computeCapital();
    }

    filterCollection() {
        this.collectionsFiltered = this.collections.filter(collection => {
            // console.log('toDate', '' + collection.dateCreated.toDate().getUTCFullYear());
            return '' + collection.date.substring(0, 4) == this.date.substring(0, 4);
        });

        this.computeCollection();
    }



    getToday() {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        this.dateToday = year + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? '0' : '') + day;
        this.date = year + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? '0' : '') + day;
        console.log(this.dateToday);
    }

    generateChart() {
        this.lineChart = new Chart(this.lineCanvas.nativeElement, {
            type: "line",
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "Novemeber", "December"],
                datasets: [
                    {
                        label: "My First dataset",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: "butt",
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: this.incomeMonthly,
                        spanGaps: false
                    }
                ]
            }
        });
    }

    async listenToLoans() {
        this.loading = true;
        firebase.firestore().collection('loans')
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id, fullName: doc.data().profile.firstName + ' ' + (doc.data().profile.middleName ? doc.data().profile.middleName + ' ' : '') + doc.data().profile.lastName })
                });
                console.log('new data', data);
                this.zone.run(() => {
                    this.loading = false;
                    this.loans = data
                })
                this.filterLoans();
            })
    }


    async listenToCollection() {
        this.loading = true;
        firebase.firestore().collection('collection')
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id })
                });
                console.log('new data', data);
                this.zone.run(() => {
                    this.loading = false;
                    this.collections = data
                })
                this.filterCollection();
            })
    }


    computeCapital() {
        this.totalCapital = 0;
        this.loansFiltered.map(loan => {
            if (['approved', 'done'].includes(loan.status)) {
                this.totalCapital += loan.principal
            }
        })
        this.monthlyCapital();
    }


    computeCollection() {
        this.totalCollection = 0;
        this.totalIncome = 0;
        this.collectionsFiltered.map(collection => {
            this.totalCollection += collection.amount
        })
        if (this.totalCapital) {
            this.totalIncome = this.totalCollection - this.totalCapital;
        } else {
            setTimeout(() => {
                this.totalIncome = this.totalCollection - this.totalCapital;
            }, 2000);
        }
        this.monthlyCollection();

    }

    monthlyCapital() {
        this.capitalMonthly = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.loansFiltered.map(loan => {
            if (['approve', 'done'].includes(loan.status)) {
                this.capitalMonthly[loan.dateCreated.toDate().getUTCMonth()] += loan.principal;
            }
        })
        // console.log('filtered', this.capitalMonthly);
    }


    monthlyCollection() {
        this.collectionMonthly = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.collectionsFiltered.map(collection => {
            console.log('date', collection.date, collection.date.substring(5, 7));
            this.collectionMonthly[parseInt(collection.date.substring(5, 7), 10) - 1] += collection.amount;
        })
        // console.log('filtered', this.collectionMonthly);
        this.monthlyIncome();
    }

    monthlyIncome() {
        this.incomeMonthly = this.incomeMonthly.map((income, i) => {
            return this.collectionMonthly[i] - this.capitalMonthly[i];
        })
        console.log('monthly', this.incomeMonthly);
        // this.lineChart.data = this.incomeMonthly;
        this.generateChart();

    }

    print() {
        var data = document.getElementById('lineCanvas');

        const option = {allowTaint: true, useCORS: true};
        html2canvas(data, option).then(canvas => {
            // Few necessary setting options  
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
            var position = 0;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            pdf.save('MYPdf.pdf'); // Generated PDF   

            
        });
    }

}
