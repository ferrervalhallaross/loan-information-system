import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralJournalPage } from './general-journal.page';

const routes: Routes = [
  {
    path: '',
    component: GeneralJournalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralJournalPageRoutingModule {}
