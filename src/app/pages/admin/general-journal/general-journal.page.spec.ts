import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeneralJournalPage } from './general-journal.page';

describe('GeneralJournalPage', () => {
  let component: GeneralJournalPage;
  let fixture: ComponentFixture<GeneralJournalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralJournalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneralJournalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
