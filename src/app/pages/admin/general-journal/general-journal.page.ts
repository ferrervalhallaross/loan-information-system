import { Component, OnInit, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/providers/admin.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-general-journal',
  templateUrl: './general-journal.page.html',
  styleUrls: ['./general-journal.page.scss'],
})
export class GeneralJournalPage implements OnInit {
    loading = false;
    loans = [];
    collections = [];
    loansFiltered = [];
    collectionsFiltered = [];
    date = '';
    dateToday = '';

    viewBy = "monthly"
    year: any;
    month: any;

    data = [];

    constructor(
        public afAuth: AngularFireAuth,
        private zone: NgZone,
        private router: Router,
        private adminService: AdminService
    ) { }

    ngOnInit() {
        this.listenToLoans();
        this.listenToCollection();
        this.getToday();
    }

    filterLoans() {
        this.loansFiltered = this.loans.filter(loan => {
            // console.log('toDate', '' + loan.dateCreated.toDate().getUTCFullYear());
            if(this.viewBy == 'yearly') {
                return '' + loan.dateCreated.toDate().getUTCFullYear() == this.date.substring(0, 4);
            } else {
                console.log('withmonth', '' + loan.dateCreated.toDate().getUTCFullYear() + loan.dateCreated.toDate().getUTCMonth());
                
                console.log('withmonth 2',this.date.substring(0, 7));
                
                var month = loan.dateCreated.toDate().getUTCMonth()  + 1; //months from 1-12
                return '' + loan.dateCreated.toDate().getUTCFullYear() +  '-' + (month < 10 ? '0' : '') + month == this.date.substring(0, 7);
            }
        });
        if(this.collectionsFiltered.length) {
            this.combine();
        }
        return this.loansFiltered
    }

    filterCollection() {
        this.collectionsFiltered = this.collections.filter(collection => {
            // console.log('toDate', '' + collection.dateCreated.toDate().getUTCFullYear());
            // return '' + collection.date.substring(0, 4) == this.date.substring(0, 4);
            if(this.viewBy == 'yearly') {
                return '' + collection.date.substring(0, 4) == this.date.substring(0, 4);
            } else {
                return '' + collection.date.substring(0, 7) == this.date.substring(0, 7);
            }
        });
        if(this.loansFiltered.length) {
            this.combine();
        }
        return this.collectionsFiltered
    }

    combine() {
        console.log('combine', this.collectionsFiltered, this.loansFiltered);
        
        this.data = [
            ...this.collectionsFiltered.map(collection => {
                return {
                    ...collection,
                    dateFinal: new Date(collection.date),
                    isCollection: true,
                    isLoan: false,
                }
            }),
            ...this.loansFiltered.map(loan => {
                return {
                    ...loan,
                    dateFinal: loan.dateCreated.toDate(),
                    isCollection: false,
                    isLoan: true,
                }
            }),

        ].sort((a, b) => b.dateFinal - a.dateFinal)
        console.log('data', this.data);
        
    }



    getToday() {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        this.dateToday = year + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? '0' : '') + day;
        this.date = year + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? '0' : '') + day;
        console.log(this.dateToday);
    }

    async listenToLoans() {
        this.loading = true;
        firebase.firestore().collection('loans').where('status', 'in', ['approved', 'done']).orderBy("dateCreated", "desc")
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id, fullName: doc.data().profile.firstName + ' ' + (doc.data().profile.middleName ? doc.data().profile.middleName + ' ' : '') + doc.data().profile.lastName })
                });
                console.log('listenToLoans', data);
                this.zone.run(() => {
                    this.loading = false;
                    this.loans = data
                })
                this.filterLoans();
            })
    }


    async listenToCollection() {
        this.loading = true;
        firebase.firestore().collection('collection').orderBy("date", "desc")
            .onSnapshot((querySnapshot) => {
                const data = []
                querySnapshot.forEach(function (doc) {
                    data.push({ ...doc.data(), _id: doc.id })
                });
                console.log('listenToCollection', data);
                this.zone.run(() => {
                    this.loading = false;
                    this.collections = data
                })
                this.filterCollection();
            })
    }

    segmentChanged(e) {
        console.log('segmentChanged', e);
        this.zone.run(() => {
            this.viewBy = e.detail.value;
        })
    }

    dateChange() {
        console.log('dateChange', this.date, this.viewBy);
        this.filterCollection();
        this.filterLoans(); 
        console.log('filtered', this.collectionsFiltered, this.loansFiltered);
    }

}
