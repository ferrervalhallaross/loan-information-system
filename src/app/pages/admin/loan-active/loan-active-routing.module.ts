import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanActivePage } from './loan-active.page';

const routes: Routes = [
  {
    path: '',
    component: LoanActivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoanActivePageRoutingModule {}
