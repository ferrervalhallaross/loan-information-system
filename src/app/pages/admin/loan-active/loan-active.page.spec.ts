import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoanActivePage } from './loan-active.page';

describe('LoanActivePage', () => {
  let component: LoanActivePage;
  let fixture: ComponentFixture<LoanActivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanActivePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoanActivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
