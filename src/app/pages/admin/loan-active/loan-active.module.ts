import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoanActivePageRoutingModule } from './loan-active-routing.module';

import { LoanActivePage } from './loan-active.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoanActivePageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [LoanActivePage]
})
export class LoanActivePageModule {}
