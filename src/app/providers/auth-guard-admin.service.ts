import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardAdminService {
    constructor(
        public auth: AuthService,
        public router: Router,
        private menu: MenuController
    ) { }
    async canActivate(): Promise<boolean> {
        //console.log('AuthGuard', !this.auth.isAuthenticated());

        return this.auth.isAdmin().then(token => {
            console.log('isAdmin', token);
            
            if (!token) {
                this.router.navigate(['login']);
                return false;
            }

            this.menu.enable(true, 'admin');
            this.menu.enable(false, 'client');
            return true;

        }).catch(error => {
            console.error(error);
            this.router.navigate(['login']);
            return false;
        });

    }
}
