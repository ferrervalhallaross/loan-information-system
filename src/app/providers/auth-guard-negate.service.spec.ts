import { TestBed } from '@angular/core/testing';

import { AuthGuardNegateService } from './auth-guard-negate.service';

describe('AuthGuardNegateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardNegateService = TestBed.get(AuthGuardNegateService);
    expect(service).toBeTruthy();
  });
});
