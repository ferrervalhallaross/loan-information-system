import { Injectable } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { MenuController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardNegateService {

    constructor(
        public auth: AuthService,
        public router: Router,
        private menu: MenuController
    ) { }
    canActivate(): Promise<boolean> {
        console.log('here canActivate');
        
        return this.auth.getBorrowerId().then(token => {
            console.log('borrowerId', token);
            
            if (token) {

                this.menu.enable(false, 'admin');
                this.menu.enable(true, 'client');
                this.router.navigate(['home']);
                return false;
            } else {
                this.auth.isAdmin().then(token => {
                    if (token) {

                        this.menu.enable(true, 'admin');
                        this.menu.enable(false, 'client');
                        this.router.navigate(['loan-applications']);
                        return false;
                    }
                }).catch(error => {
                    console.error(error);
                    this.menu.enable(false, 'admin');
                    this.menu.enable(false, 'client');
                    this.router.navigate(['login']);
                    return false;
        
                });
            }

            return true;

        }).catch(error => {

            console.error(error);
            this.router.navigate(['login']);
            return false;

        });

    }
}
