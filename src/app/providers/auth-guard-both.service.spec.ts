import { TestBed } from '@angular/core/testing';

import { AuthGuardBothService } from './auth-guard-both.service';

describe('AuthGuardBothService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardBothService = TestBed.get(AuthGuardBothService);
    expect(service).toBeTruthy();
  });
});
