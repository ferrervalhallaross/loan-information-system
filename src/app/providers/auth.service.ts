import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
		private firebaseService: FirebaseService,
        public afAuth: AngularFireAuth,
        private storage: Storage,
        private afs: AngularFirestore,
    ){}
    


    public getBorrowerId(): Promise<string> {
        return this.storage.get('borrowerId');
    }

    public isAdmin(): Promise<string> {
        return this.storage.get('isAdmin');
    }

    public storageRemoveBorrowerId(): Promise<string> {
        return this.storage.remove('borrowerId');
    }

    public storageRemoveIsAdmin(): Promise<string> {
        return this.storage.remove('isAdmin');
    }


    public setToken(token: string): Promise<any> {
        console.log(token);
        return this.storage.set('IFAE_TOKEN', token);
        // Check whether the token is expired and return
        // true or false
    }

    doRegister(value){  
        return new Promise<any>((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
            .then(
            res => resolve(res),
            err => reject(err))
        })
    }

    addBorrower(body, id){  
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('borrower').doc(id).set(body).then(
            res => resolve(res),
            err => reject(err))
        })
    }

    addLoan(body){  
        let id = this.afs.createId();
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('loans').doc(id).set(body).then(
            res => resolve(res),
            err => reject(err))
        })
    }

    getLoanCount(){  
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('constants').doc('loanCount').get().then(
            doc => resolve(doc),
            err => reject(err))
        })
    }

    doLogin(value){
        return new Promise<any>((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
            .then(
            res => resolve(res),
            err => reject(err))
        })
    }

    doLogout(){
        return new Promise((resolve, reject) => {
            this.afAuth.auth.signOut()
            .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
            }).catch((error) => {
            reject(error);
            });
        })
    }



    getAdminEmail() {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('users').where("userType", "==", "super-admin")
            .get().then(function(querySnapshot) {
                resolve(querySnapshot)
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
                reject(error)
            });
        })  
    }


    checkIfBorrower(email) {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('borrower').where("email", "==", email)
            .get().then(function(querySnapshot) {
                resolve(querySnapshot)
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
                reject(error)
            });
        })  
    }


}