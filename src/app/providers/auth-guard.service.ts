import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
    constructor(
        public auth: AuthService,
        public router: Router,
        private menu: MenuController
    ) { }
    async canActivate(): Promise<boolean> {
        //console.log('AuthGuard', !this.auth.isAuthenticated());

        let borrowerID = await this.auth.getBorrowerId()
        if(borrowerID){
            this.menu.enable(false, 'admin');
            this.menu.enable(true, 'client');
        }

        let isAdmin = await this.auth.isAdmin()
        if(isAdmin){
            this.menu.enable(true, 'admin');
            this.menu.enable(false, 'client');
        }

        if (borrowerID || isAdmin) {
            return true;
        } else {
            this.router.navigate(['login']);
            return false;
        }

    }
}
