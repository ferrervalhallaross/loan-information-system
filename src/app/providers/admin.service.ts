import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    private snapshotChangesSubscription: any;

    constructor(
        public afs: AngularFirestore,
        public afAuth: AngularFireAuth,
        private http: HttpClient,
    ) { }

    postNotif(id, message = '', send_after: any = Date.now(), topic = 'general') {
        console.log('Date.now()', send_after);

        let body = {
            app_id: "f1f4af9f-f280-4cb1-bb56-ee0be91eb016",
            include_player_ids: [id],
            data: { foo: "bar" },
            contents: { en: message },
            send_after,
            web_push_topic: topic
        }
        return this.http.post(`https://onesignal.com/api/v1/notifications`, body).toPromise()
    }

    approveLoan(id) {
        return this.afs.doc<any>('loans/' + id).update({
            status: "approved"
        })
    }

    declineLoan(id) {
        return this.afs.doc<any>('loans/' + id).update({
            status: "declined"
        })
    }

    cancelLoan(id) {
        return this.afs.doc<any>('loans/' + id).update({
            status: "cancelled"
        })
    }

    doneLoan(id) {
        return this.afs.doc<any>('loans/' + id).update({
            status: "done"
        })
    }

    postCollection({ date, amount, accountNo, borrowerID, loanID }) {
        return new Promise<any>((resolve, reject) => {
            this.afs.collection('collection').add({
                date,
                amount,
                accountNo,
                borrowerID,
                loanID
            })
                .then(
                    res => resolve(res),
                    err => reject(err)
                )
        })
    }

    getAdminOneSignalID() {
        return new Promise<any>((resolve, reject) => {
            firebase.firestore().collection('users').where("userType", "==", "super-admin")
                .get().then(function (querySnapshot) {
                    resolve(querySnapshot)
                })
                .catch(function (error) {
                    console.log("Error getting documents: ", error);
                    reject(error)
                });
        })
    }

}