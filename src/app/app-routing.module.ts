import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardNegateService } from './providers/auth-guard-negate.service';
import { AuthGuardService } from './providers/auth-guard.service';
import { AuthGuardAdminService } from './providers/auth-guard-admin.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'signup',
    loadChildren: () => import('./pages/client/signup/signup.module').then( m => m.SignupPageModule),
    canActivate: [AuthGuardNegateService]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/client/login/login.module').then( m => m.LoginPageModule),
    canActivate: [AuthGuardNegateService]
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/client/home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/admin/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'excel',
    loadChildren: () => import('./pages/excel/excel.module').then( m => m.ExcelPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/client/profile/profile.module').then( m => m.ProfilePageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'loans',
    loadChildren: () => import('./pages/client/loans/loans.module').then( m => m.LoansPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/client/settings/settings.module').then( m => m.SettingsPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'loan-history',
    loadChildren: () => import('./pages/admin/loan-history/loan-history.module').then( m => m.LoanHistoryPageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'loan-applications',
    loadChildren: () => import('./pages/admin/loan-applications/loan-applications.module').then( m => m.LoanApplicationsPageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'apply-loan',
    loadChildren: () => import('./pages/client/apply-loan/apply-loan.module').then( m => m.ApplyLoanPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'loan-view',
    loadChildren: () => import('./pages/admin/loan-view/loan-view.module').then( m => m.LoanViewPageModule),
    canActivate: [AuthGuardService],
  },
  {
    path: 'admin-settings',
    loadChildren: () => import('./pages/admin/admin-settings/admin-settings.module').then( m => m.AdminSettingsPageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'loan-active',
    loadChildren: () => import('./pages/admin/loan-active/loan-active.module').then( m => m.LoanActivePageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'post-collection',
    loadChildren: () => import('./pages/admin/post-collection/post-collection.module').then( m => m.PostCollectionPageModule),
    canActivate: [AuthGuardAdminService],
  },
  {
    path: 'statistics',
    loadChildren: () => import('./pages/admin/statistics/statistics.module').then( m => m.StatisticsPageModule)
  },
  {
    path: 'couple',
    loadChildren: () => import('./pages/couple/couple.module').then( m => m.CouplePageModule)
  },
  {
    path: 'payment-options',
    loadChildren: () => import('./pages/client/payment-options/payment-options.module').then( m => m.PaymentOptionsPageModule)
  },
  {
    path: 'general-journal',
    loadChildren: () => import('./pages/admin/general-journal/general-journal.module').then( m => m.GeneralJournalPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
